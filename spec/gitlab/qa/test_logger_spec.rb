# frozen_string_literal: true

describe Gitlab::QA::TestLogger do
  let(:logger) { described_class.logger }

  describe '.logger' do
    let(:console_log) { instance_double(ActiveSupport::Logger) }
    let(:file_log) { instance_double(ActiveSupport::Logger) }

    before do
      allow(described_class).to receive(:console_logger).and_return(console_log)
      allow(described_class).to receive(:file_logger).and_return(file_log)
    end

    it 'creates a console logger' do
      expect(described_class).to receive(:console_logger)

      described_class.logger
    end

    it 'creates a file logger' do
      expect(described_class).to receive(:file_logger)

      described_class.logger
    end

    context 'when ActiveSupport::BroadcastLogger is defined' do
      before do
        klass = Class.new do
          def initialize(*loggers); end
        end

        stub_const('ActiveSupport::BroadcastLogger', klass)
      end

      it 'returns a BroadcastLogger with the console and file loggers' do
        expect(ActiveSupport::BroadcastLogger).to receive(:new).with(console_log, file_log)

        described_class.logger
      end
    end

    context 'when Logger supports broadcasting' do
      let(:klass) do
        Class.new do
          def self.broadcast(*args); end
        end
      end

      before do
        allow(ActiveSupport).to receive(:const_defined?).with('Logger', false).and_return(true)
        allow(ActiveSupport).to receive(:const_defined?).with(:BroadcastLogger).and_return(false)
        stub_const('ActiveSupport::Logger', klass)
      end

      it 'extends the console logger to broadcast to the file logger' do
        expect(console_log).to receive(:extend)
        expect(ActiveSupport::Logger).to receive(:broadcast).with(file_log)

        described_class.logger
      end
    end

    context 'when broadcasting is not supported' do
      before do
        allow(ActiveSupport).to receive(:const_defined?).with(:BroadcastLogger).and_return(false)
        allow(ActiveSupport::Logger).to receive(:respond_to?).with(:broadcast).and_return(false)
      end

      it 'raises an error' do
        expect { described_class.logger }.to raise_error('Could not configure logger broadcasting')
      end
    end
  end
end
