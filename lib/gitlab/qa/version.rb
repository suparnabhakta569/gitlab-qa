# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '14.10.0'
  end
end
