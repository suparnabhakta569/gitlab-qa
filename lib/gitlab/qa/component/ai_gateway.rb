# frozen_string_literal: true

module Gitlab
  module QA
    module Component
      class AiGateway < Base
        DOCKER_IMAGE = 'registry.gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/model-gateway'
        DOCKER_IMAGE_TAG = 'latest'
        LOG_DIR = '/var/log'

        def name
          @name ||= 'ai-gateway'
        end

        def log_volume
          @log_volume ||= {
            src: File.join(Runtime::Env.host_artifacts_dir, @name, 'logs'),
            dest: LOG_DIR
          }
        end

        def configure_environment(gitlab_hostname:)
          @environment = {
            'AIGW_GITLAB_URL' => "http://#{gitlab_hostname}",
            'AIGW_GITLAB_API_URL' => "http://#{gitlab_hostname}/api/v4",
            'AIGW_CUSTOMER_PORTAL_URL' => Runtime::Env.customer_portal_url,
            'AIGW_MOCK_MODEL_RESPONSES' => true,
            'AIGW_LOGGING__LEVEL' => 'debug',
            'AIGW_LOGGING__TO_FILE' => "..#{LOG_DIR}/modelgateway_debug.log"
          }
        end
      end
    end
  end
end
