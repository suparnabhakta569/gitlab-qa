# frozen_string_literal: true

class DuoProSetup
  class << self
    def configure!
      activate_cloud_license

      return unless enabled?('HAS_ADD_ON')

      # Due to the various async Sidekiq processes involved, we wait to verify
      # that the service access token has been generated before proceeding
      verify_service_access_token

      assign_duo_pro_seat_to_admin if enabled?('ASSIGN_SEATS')
    end

    private

    def activate_cloud_license
      puts 'Activating cloud license...'
      result = ::GitlabSubscriptions::ActivateService.new.execute(ENV.fetch('QA_EE_ACTIVATION_CODE'))

      if result[:success]
        puts 'Cloud license activation successful'
      else
        puts 'Cloud license activation failed!'
        puts Array(result[:errors]).join(' ')
        exit 1
      end
    end

    def verify_service_access_token
      puts 'Waiting for service access token to be available...'

      max_attempts = 3
      attempts = 0

      until (tokens = ::CloudConnector::ServiceAccessToken.active.count)&.positive? || attempts == max_attempts
        puts 'Attempting to verify access token exists...'
        attempts += 1
        sleep 30
      end

      return if tokens&.positive?

      puts "Failed to create service access token after #{max_attempts} attempts"
      exit 1
    end

    def assign_duo_pro_seat_to_admin
      puts 'Assigning Duo Pro seat to admin...'

      admin = User.find_by(username: 'root')
      add_on = GitlabSubscriptions::AddOnPurchase.find_by(add_on: GitlabSubscriptions::AddOn.code_suggestions.last)

      result = ::GitlabSubscriptions::UserAddOnAssignments::SelfManaged::CreateService.new(
        add_on_purchase: add_on, user: admin
      ).execute

      if result.is_a?(ServiceResponse) && result[:status] == :success
        puts 'Seat assignment for admin successful'
      else
        puts 'Seat assignment for admin failed!'

        error = result.is_a?(ServiceResponse) ? result[:message] : result
        puts error
      end
    end

    def enabled?(key, default: nil)
      ENV.fetch(key, default) == 'true'
    end
  end
end

DuoProSetup.configure!
